#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"

#include "floor_1.h"

// C++ include files
#include<iostream>
#include <fstream>



namespace behave{


Behave::Behave(){
    ros::NodeHandle n;

    //for comanding the base
    vel_pub_ = n.advertise<geometry_msgs::Twist>("cmd_vel", 1);

    //ros::Subscriber action_sub = n.subscribe("cmd_vel", 100, cmd_vel_Callback);

    laser_sub = n.subscribe("scan", 1, &Behave::laser_Callback,this);

/*----------------------------------------*/
    cmd_vel_recover.linear.x = -0.1;
    cmd_vel_recover.linear.y = 0;
    cmd_vel_recover.linear.z = 0;

    cmd_vel_recover.angular.x = 0;
    cmd_vel_recover.angular.y = 0;
    cmd_vel_recover.angular.z = -2;

/*----------------------------------------*/
    cmd_vel_go_straight.linear.x = 0.3;
    cmd_vel_go_straight.linear.y = 0;
    cmd_vel_go_straight.linear.z = 0;

    cmd_vel_go_straight.angular.x = 0;
    cmd_vel_go_straight.angular.y = 0;
    cmd_vel_go_straight.angular.z = 0;
/*----------------------------------------*/
    cmd_vel_turn_right.linear.x = 0;
    cmd_vel_turn_right.linear.y = 0;
    cmd_vel_turn_right.linear.z = 0;

    cmd_vel_turn_right.angular.x = 0;
    cmd_vel_turn_right.angular.y = 0;
    cmd_vel_turn_right.angular.z = -1.5;
/*----------------------------------------*/
    cmd_vel_turn_left.linear.x = 0;
    cmd_vel_turn_left.linear.y = 0;
    cmd_vel_turn_left.linear.z = 0;

    cmd_vel_turn_left.angular.x = 0;
    cmd_vel_turn_left.angular.y = 0;
    cmd_vel_turn_left.angular.z = 1.5;
/*----------------------------------------*/

}





void Behave::laser_Callback(const sensor_msgs::LaserScanConstPtr& laser_scan){


    bool recovery_flag = false;

    max_beams = laser_scan->ranges.size();


    double beam_sum = 0;

    //ROS_INFO("%d", max_beams);

    for(int beam_no = 0 ; beam_no < max_beams ; beam_no++){
        beam_sum += laser_scan->ranges[beam_no];
        if(laser_scan->ranges[beam_no] < (laser_scan->range_min + 0.5)){
            recovery_flag = true;
        }
    }

    straight_prob = (beam_sum/(laser_scan->range_max * max_beams * 0.5 )) * 100;
    right_prob = (100 - straight_prob)/2;
    left_prob = 100 - (straight_prob + right_prob);

    double dice_roll = drand48()*100;
    ROS_INFO("straight_prob:%f || dice_roll:%f", straight_prob, dice_roll);
    //ROS_INFO("left: %f | straight: %f | right: %f", left_prob, straight_prob, right_prob );



    if(recovery_flag){
        vel_pub_.publish(cmd_vel_recover);
    }
    else{

        if(dice_roll >=0 && dice_roll < straight_prob){ //Go Straight
            vel_pub_.publish(cmd_vel_go_straight);
        }
        else{
            if(dice_roll >= straight_prob && dice_roll < (straight_prob + right_prob) ){ //Turn Right
                vel_pub_.publish(cmd_vel_turn_right);
            }
            else{ //Turn Left
                vel_pub_.publish(cmd_vel_turn_left);
            }
        }
    }




    ros::Rate loop_rate(0.4);

    loop_rate.sleep();

}

}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "behave");

  behave::Behave behaviour;


  ros::spin();

  return 0;
}
