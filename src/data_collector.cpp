#include "data_collector.h"



void data_collector::experimentState_callback(const std_msgs::String::ConstPtr& experimentState_msg){

    if (!shutting_down){
        if(experiment_state.compare(experimentState_msg->data) != 0){
            ROS_INFO("data_collector received experiment_state: ## %s ##", experimentState_msg->data.c_str());
            experiment_state = experimentState_msg->data;
        }

        std::size_t found1, found2;

        found1 = experiment_state.find("crashed");
        if(found1 != std::string::npos){
            remove(filename_abs.c_str());
            shutting_down = true;
            ros::shutdown();
        }

        found1 = experiment_state.find("stopping");
        found2 = experiment_state.find("timed out");
        if(found1 != std::string::npos
                || found2 != std::string::npos){


            shutting_down = true;

            if(file_created){
                if(!headers_written){

                    data_collection_fstream->open(filename_abs.c_str(), std::ios::app);

                    if (data_collection_fstream->is_open()) {
                        *data_collection_fstream << headers << ",Robot1 Experiment State" << "\n"
                                                 << data << "," << experiment_state<<"\n";
                        data_collection_fstream->close();
                        headers_written = true;
                    }
                    else{
                        printf("\n ********** Filestream opening ERROR while writing data in data_collector::npf_data_callback() !!! ********** \n");

                        std::cin.ignore(); //just awaiting a keypress of "Enter" to
                        //proceed just to make sure this is read
                    }
                }
                else{

                    data_collection_fstream->open(filename_abs.c_str(), std::ios::app);

                    if (data_collection_fstream->is_open()) {
                        *data_collection_fstream << data << "," << experiment_state<<"\n";
                        data_collection_fstream->close();
                    }
                    else{
                        printf("\n ********** Filestream opening ERROR while writing data in data_collector::npf_data_callback() !!! ********** \n");

                        std::cin.ignore(); //just awaiting a keypress of "Enter" to
                        //proceed just to make sure this is read
                    }
                }
            }

            ros::shutdown();


        }

    }


}

void data_collector::npf_filename_callback(const std_msgs::String::ConstPtr& filename_msg){
    //ROS_INFO("data_collector received: ## %s ##", npf_msg->data.c_str());
    filename_abs = filename_msg->data;

    if(!file_created){
        data_collection_fstream->open(filename_abs.c_str());

        if (data_collection_fstream->is_open()) {
            printf("-----Filestream opened successfully-----\n");

            data_collection_fstream->close();

            file_created = true;
        }
        else{
            printf("\n ********** Filestream opening ERROR!!! ********** \n");

            std::cin.ignore(); //just awaiting a keypress of "Enter" to
            //proceed just to make sure this is read
            ros::shutdown();
        }
    }
}

void data_collector::npf_headers_callback(const std_msgs::String::ConstPtr& headers_msg){
    //ROS_INFO("data_collector received: ## %s ##", npf_msg->data.c_str());
    headers = headers_msg->data;
}

void data_collector::npf_data_callback(const std_msgs::String::ConstPtr& npf_msg){
    data = npf_msg->data;

    ROS_DEBUG("\n data_collector received:"
             "\n\t filename_abs : %s "
             "\n\t headers : %s "
             "\n\t data : %s ",
             filename_abs.c_str(),
             headers.c_str(),
             data.c_str());

    if(file_created){
        if(!headers_written && (headers.compare(std::string("")) != 0) ){

            data_collection_fstream->open(filename_abs.c_str(), std::ios::app);

            if (data_collection_fstream->is_open()) {
                *data_collection_fstream << headers << ",Robot1 Experiment State" << "\n"
                                         << data << "," << experiment_state<<"\n";
                data_collection_fstream->close();
                headers_written = true;
            }
            else{
                printf("\n ********** Filestream opening ERROR while writing data in data_collector::npf_data_callback() !!! ********** \n");

                std::cin.ignore(); //just awaiting a keypress of "Enter" to
                //proceed just to make sure this is read
                ros::shutdown();
            }

        }
        else{

            data_collection_fstream->open(filename_abs.c_str(), std::ios::app);

            if (data_collection_fstream->is_open()) {
                *data_collection_fstream << data << "," << experiment_state<<"\n";
                data_collection_fstream->close();
            }
            else{
                printf("\n ********** Filestream opening ERROR while writing data in data_collector::npf_data_callback() !!! ********** \n");

                std::cin.ignore(); //just awaiting a keypress of "Enter" to
                //proceed just to make sure this is read
                ros::shutdown();
            }
        }
    }
}

data_collector::data_collector()
{

    ros::NodeHandle n;

    npf_data_sub_ = n.subscribe("/robot1/nested_amcl_data", 2, &data_collector::npf_data_callback, this);
    npf_filename_sub_ = n.subscribe("/robot1/data_file_name", 2, &data_collector::npf_filename_callback, this);
    npf_headers_sub_ = n.subscribe("/robot1/data_headers", 2, &data_collector::npf_headers_callback, this);
    robot1_exp_state_sub_ = n.subscribe("/robot1_experiment_state", 2, &data_collector::experimentState_callback, this);

    data_collection_fstream = new std::ofstream;

    file_created = false;
    headers_written = false;

    shutting_down = false;
}

data_collector::~data_collector()
{
    delete data_collection_fstream;
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "data_collector");

    data_collector collector;


    ros::spin();

    return 0;
}
