#ifndef DATA_COLLECTOR_H
#define DATA_COLLECTOR_H

#include<ros/ros.h>
#include<std_msgs/String.h>
#include <fstream>

class data_collector
{
public:
    data_collector();
    ~data_collector();


    void experimentState_callback(const std_msgs::String::ConstPtr& experimentState_msg);
    void npf_data_callback(const std_msgs::String::ConstPtr& npf_msg);
    void npf_filename_callback(const std_msgs::String::ConstPtr& filename_msg);
    void npf_headers_callback(const std_msgs::String::ConstPtr& headers_msg);


private:
    ros::Subscriber npf_data_sub_;
    ros::Subscriber npf_filename_sub_;
    ros::Subscriber npf_headers_sub_;
    ros::Subscriber robot1_exp_state_sub_;

    std::string filename_abs, headers, data, experiment_state;

    std::ofstream *data_collection_fstream;

    bool file_created, headers_written, shutting_down;
};

#endif // DATA_COLLECTOR_H
